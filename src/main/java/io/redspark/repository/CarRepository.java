package io.redspark.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import io.redspark.domain.Car;

@RepositoryRestResource(collectionResourceRel = "car", path="car")
public interface CarRepository extends PagingAndSortingRepository<Car, Long>{

}
